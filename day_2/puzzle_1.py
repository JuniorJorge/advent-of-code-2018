def main():
    with open('input.txt') as file:
        data = file.readlines()
    input = [line.strip() for line in data]

    two_letter = 0
    three_letter = 0
    for box_id in input:
        char_dict = dict()
        for char in box_id:
            if char in char_dict.keys():
                char_dict[char] += 1
            else:
                char_dict[char] = 1

        twos = [key for key, val in char_dict.items() if val == 2]
        threes = [key for key, val in char_dict.items() if val == 3]
        two_letter += 1 if len(twos) > 0 else 0
        three_letter += 1 if len(threes) > 0 else 0

    print(two_letter * three_letter)


if __name__ == '__main__':
    main()
