def main():
    with open('input.txt') as file:
        data = file.readlines()
    input = [line.strip() for line in data]
    input.sort()

    one = False
    for i in range(0, len(input) - 1):
        for letters in zip(input[i], input[i + 1]):
            same = set(letters)
            if len(same) == 2:
                if one:
                    one = False
                    break
                else:
                    one = True
        if one:
            print(input[i])
            print(input[i+1])
        





if __name__ == '__main__':
    main()
