def main():
    with open('input.txt') as file:
        data = file.readlines()
    input = [line.strip() for line in data]

    not_found = True
    frequency = 0
    freqs = set([frequency])
    repeat = None

    while not_found:
        for change in input:
            frequency += int(change)
            
            if frequency in freqs:
                not_found = False
                repeat = frequency
                break

            freqs.add(frequency)

    print(repeat)


if __name__ == '__main__':
    main()
