def main():
    with open('input.txt') as file:
        data = file.readlines()

    input = [line.strip() for line in data]

    frequency = 0
    for change in input:
        frequency += int(change)

    print(frequency)


if __name__ == '__main__':
    main()
